import network
import os
import utime
from machine import Pin,Timer
p0=Pin(0,Pin.OUT)
#p1=Pin(1,Pin.IN)
p2=Pin(2,Pin.OUT)
p3=Pin(3,Pin.OUT)
# ssd1306 0.96 128*64 显示屏封装
#from display import ctx

# 时间 显示封装
#from timestring import timeEvent

# 判断网络
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.disconnect()
utime.sleep(1)
if not wifi.isconnected():
    # 没联网
    try:
        file = open("/wificonfig.txt", mode='r',encoding="utf-8")
        try :
            ssid = file.readline().replace("ssid:","").replace("\n","").replace("\r","").replace(" ","")
            password = file.readline().replace("password:","").replace("\n","").replace("\r","").replace(" ","")
        finally :
            file.close()
        while True:
            wifi.connect(ssid, password)
            print('正在连接中！！')
            utime.sleep(15)
            print(wifi.ifconfig())
            if wifi.isconnected():
                break
        print(wifi.ifconfig())
        print(wifi.isconnected())
    except Exception as e:
        from wifisetup import WIFISETUP 
        WIFISETUP()

print(wifi.ifconfig())
utime.sleep(1)

from simple import MQTTClient
import ujson
def sub_cb(topic, msg):
    print('--------------------')
    print('topic: {}'.format(topic))
    print('msg: {}'.format(msg))
    print('--------------------')
    command=ujson.loads(msg.decode())
    if topic.decode() == '/hrow8srVwwV/esp01/user/get':
        # 主动推送，需要处理数据
        print('------111111') # {"data":{"p0":1}}
        try:
            data = command['data']
            try:
                if data["p1"] == 0: 
                    #p1.value(0)
                    pass
                else :
                    #p1.value(1)
                    pass
            except:
                pass
            try:
                if data["p3"] == 0: 
                    p3.value(0)
                else :
                    p3.value(1)
            except:
                pass
            try:
                if data["p2"] == 0: 
                    p2.value(0)
                else :
                    p2.value(1)
            except:
                pass
            try:
                if data["p0"] == 0: 
                    p0.value(0)
                else :
                    p0.value(1)
            except:
                pass
        except:
            pass
    if topic.decode() == '/sys/hrow8srVwwV/esp01/thing/event/property/post_reply':
        print('every 5 seconds resolve')
    try:
        stopcode=command['params']['aa']
        if stopcode==0:
            print('云端控制开始')
        elif stopcode==1:
            print('云端控制停止')
    except:
        pass

def MQTT_Rev(tim):
    #client.check_msg()
    print('gogogo')
        
def MQTT_Publish(tim):
    mymessage='{"params": {"msg":""}, "method": "thing.event.property.post"}'
    print(mymessage)
    client.publish(publish_TOPIC, mymessage,qos=0)
    client.check_msg()
    
try:
    file = open("/mqttconfig.txt", mode='r',encoding="utf-8")
    try :
        clientId = file.readline().replace("clientId:","").replace("\n","").replace("\r","").replace(" ","")
        username = file.readline().replace("username:","").replace("\n","").replace("\r","").replace(" ","")
        mqttHostUrl = file.readline().replace("mqttHostUrl:","").replace("\n","").replace("\r","").replace(" ","")
        passwd = file.readline().replace("passwd:","").replace("\n","").replace("\r","").replace(" ","")
        #port = file.readline().replace("port:","").replace("\n","").replace("\r","").replace(" ","")
        file.close()
    finally :
        pass
    
except Exception as e:
    print('配置文件错误')
    pass

info={
    "clientId":clientId,
    "username":username,
    "mqttHostUrl":mqttHostUrl,
    "passwd":passwd,
    "port":1883
}
print(info)
subscribe_TOPIC='/hrow8srVwwV/esp01/user/get'
publish_TOPIC = '/sys/hrow8srVwwV/esp01/thing/event/property/post'
client = MQTTClient(
    info["clientId"],
    info["mqttHostUrl"],
    info["port"],
    info["username"],
    info["passwd"],
    keepalive=600
)
client.set_callback(sub_cb)
client.connect()
client.subscribe(subscribe_TOPIC)
mymessage='{"params": {"Power": {},"WF": {}},"method": "thing.event.property.post"}'
client.publish(publish_TOPIC, mymessage)

#tim = Timer(-1)
#tim.init(period=5000, mode=Timer.PERIODIC,callback=MQTT_Publish)


while True:
    utime.sleep(4)
    # 获取引脚状态
    timetmp = str(utime.mktime(utime.localtime()) + 946656000)+'000'
    mymessage='{"params": {"temperature": {"value": 25,"time": '+timetmp+'},"p2": {"value": '+str(p2.value())+',"time": '+timetmp+'},"p0": {"value": '+str(p0.value())+',"time": '+timetmp+'},"p3": {"value": '+str(p3.value())+',"time": '+timetmp+'}}, "method": "thing.event.property.post"}'
    print(mymessage)
    client.publish(publish_TOPIC, mymessage,qos=0)
    client.check_msg()
    #client.wait_msg()
    p2.on()
    utime.sleep(1)
    p2.off()