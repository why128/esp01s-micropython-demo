from machine import Pin, I2C
import lib as lib
from lib import ssd1306

class __DISPLAY__():
    def __init__(self):
        self.i2c = I2C(scl=Pin(1), sda=Pin(3),freq=100000)
        self.display = ssd1306.SSD1306_I2C(128, 64, self.i2c)


ctx = __DISPLAY__()