# wificonfig  ssid password

# mqttconfig  阿里云 对应直连的配置参数

# 实现功能  阿里云mqtt 控制esp01s p0(GPIO0) p2(GPIO2/led灯) p3(RXD)三个引脚的状态开关
```
/*
* /{ProductKey}}/{DeviceName}/user/get
*/
// mqtt 发布消息 模板 
{
    "data": {
        "p3": 1
    }
}

```
## 核心文件

> display.py

+ OLED 0.96 128*64 基于i2c四脚显示器接入

> maincopy.py

+ 便于开发，修改的文件名，原名 main.py,项目完成后，可改成main.py,就相当于完成烧录

> simple.py

+ 基于 umqtt.simple 的官方库 用来连接阿里云mqtt

> timestring.py

+ 时间格式处理

> wifisetup.py

### 初次使用，没有联网，该模块 创建了 wattley-esp01s，密码 无的网络，可通过手机连接该网络后，用浏览器访问192.168.4.1:8080 来设置 wifi，设置完成后会自动生成wificonfig.txt，重启esp01s模块即可连接上wifi，初始化时建议删除wifi，设置完成后会自动生成wificonfig.txt文件，或者 直接创建该文件，填写WiFi的名称和密码，无需用手机设置

