# auth: wattley
# time: 2022-10-06
from machine import RTC
import ntptime

rtc = RTC()
def timeEvent(s1 = '-', s2 = ':'):
    s = ""
    date = rtc.datetime()
    s = s + str(date[0])
    for item in date[1:3]:
        s0 = (s1 + str(item)) if item > 9 else (s1 + '0' + str(item))
        s = s + s0
    s = s + ' '
    s0 = str(date[4]) if date[4] > 9 else '0' + str(date[4])
    s = s + s0
    for item in date[5:7]:
        s0 =  (s2 + str(item)) if item > 9 else (s2 + '0' + str(item))
        s = s + s0
    return s
