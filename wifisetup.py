import socket
import network

import utime

class WIFISETUP():
    def __init__(self,essid="wattley-esp01s",password="12345678",authmode=0,host='0.0.0.0',port=8080):
        print('没网设置网络')
        # 创建一个页面 用户选择网络
        wlan_ap = network.WLAN(network.AP_IF)
        wlan_ap.active(True)
        #AUTHMODE = {0: "open", 1: "WEP", 2: "WPA-PSK", 3: "WPA2-PSK", 4: "WPA/WPA2-PSK"}
        wlan_ap.config(essid=essid, password=password,authmode=authmode)
        print(wlan_ap.ifconfig())
        
        server = socket.socket()
        addr = socket.getaddrinfo(host, port)[0][-1]
        server.bind(addr)                 # 将socket绑定到本机IP并且设定一个端口
        server.listen(100)
        
        wifi = network.WLAN(network.STA_IF)
        wifi.active(True)
        list = wifi.scan()
        print('wifi scan', list)

        html = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
        <html>
            <head><title>Document</title></head>
            <body>
                <div class="content">
                <form action="http://192.168.4.1:8080" method="get">
                    <input type="text" name="ssid" placeholder="ssid" />
                    <input type="text" name="pass" placeholder="pass" />
                    <input type="submit" value="save" />
                    <h1>ESP8266 wifi setup</h1>
                    <ul><li class='on'><h6>WIFI name</h6></li> %s </ul>
                    <h3>Over end line!!!</h3>
                </form>
                </div>
            </body>
        </html>
        """
        rows = ["<li class='on'><h6>%s</h6></li>" % (p[0]) for p in list]
        response = html%''.join(rows)
        exit = ''
        utime.sleep(3)
        while True:
            con, addr = server.accept()                   # 会一直等待，直到连接客户端成功
            
            print('连接到: ', addr)
            con.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
            con.send(response)
            #接收处理数据
            msg = con.recv(1024).decode('utf-8')      # 接受数据并按照utf-8解码
            if msg[0:4] == 'POST' :
                form = msg.split('\r\n')
                print('收到的数据类型是: ', form[-1])
            if msg[0:3] == 'GET' :
                form = msg.split('\r\n')
                endpin = form[0].find('HTTP')
                ssidpin = form[0].find('?ssid=')
                passpin = form[0].find('&pass=')
                ssidstr = form[0][ssidpin + 6:passpin].replace(' ','')
                passstr = form[0][passpin + 6:endpin].replace(' ','')
                if ssidpin > 0 and passpin > 0 and len(ssidstr) > 1 and len(passstr) > 1 :
                    #写入 wificonfig.txt
                    file = open("/wificonfig.txt", mode='w')
                    file.write("ssid: %s \n" %ssidstr)
                    file.write("password: %s \n" %passstr)
                    file.close()
                    server.close()     
            con.close()
                
